#ifndef _LSGLOBAL_H

#define _LSGLOBAL_H

// include necessary header for libraries.
#if defined(ARDUINO) && ARDUINO >= 100
	#include "Arduino.h"
#else
	#include "WProgram.h"
#endif
#include <avr/pgmspace.h>

// un-comment this to turn on debug printing to the screen or to the serial port. This will interfere with the lights but will let you troubleshoot the program logic.
//#define DEBUG 1
//#define ALWAYSREADY 1
#define DISPLAYTESTPATTERN 1

#endif // _LSGLOBAL_H
