#include "lsglobal.h"
#include "LightSpinnerString.h"
#include "ws2811.h"

// this runs the display routine forever
void LightSpinnerString::Run(byte* message, short msglen)
{
	byte* start = message; // the start of the message
	byte* currentstart = message; // where we are starting the display on this rotation
	byte* current = message; // the current pattern we are displaying
	byte* end = start + msglen; // the end of the message

  unsigned long colours [] = { 0xFF0000, 0xFFFF00, 0x00FF00, 0x00FFFF, 0x0000FF, 0xFF00FF};
  int colourindex = 0;
  int maxcolour = sizeof(colours) / sizeof(unsigned long);
  SetColour(colours[colourindex]);

	// forever
	while(true)
	{
		// while we have not reached the end of the message
		while (currentstart + DISPLAYS != end)
		{
			// wait for our sensor to fire to tell us to start the display for this rotation
			WaitForReady();

			// for the number of times we are going to display on this rotation
			for(int i = 0; i < DISPLAYS; i++)
			{
//				#ifdef DEBUG
//						Serial.println("About to display.");
//				#endif

				// set the bulbs
				Display(*current);

				// move to the next pattern
				current++;

				// delay for the time until we need to change the bulbs
					#ifdef DEBUG
						// delay for 3 seconds if we are in debug mode
//						delay(1000);
            delayMicroseconds(BETWEENDISPLAYS);
					#else
						delayMicroseconds(BETWEENDISPLAYS);
					#endif
			}

			// blank out the bulbs while we go around to the trigger point
			Display(0);

			// move the message along this much
			currentstart = currentstart + MOVEMESSAGEAMT;

			// move current back to the current start
			current = currentstart;

			// mkae sure we see a not ready before we move on
			WaitForNotReady();

		}

		// reset to beginning
		currentstart = start;
		current = start;

    colourindex++;
    if (colourindex == maxcolour) colourindex = 0;
    SetColour(colours[colourindex]);
	}
}

void LightSpinnerString::WaitForReady()
{
	#ifdef ALWAYSREADY
		return;
	#endif

	#ifdef DEBUG
			Serial.println("Waiting for ready.");
	#endif

		// wait till pin 11 is not set
		int in;
		do
		{
			// check pin 11
			in = digitalRead(11);
		} while (in == 1);

	#ifdef DEBUG
			Serial.println("Ready received.");
	#endif
}

void LightSpinnerString::WaitForNotReady()
{
	#ifdef ALWAYSREADY
		return;
	#endif

	#ifdef DEBUG
		Serial.println("Waiting for not ready.");
	#endif

	// wait till pin 11 is not set
	int in;
	do
	{
		// check pin 11
		in = digitalRead(11);
	} while (in == 0);

	#ifdef DEBUG
		Serial.println("Not ready received.");
	#endif
}

void LightSpinnerString::SetColour(unsigned long colour)
{
	_colour = colour;
}

// create a string of lights
	void LightSpinnerString::Construct()
	{
		// set the pin as an output pin
		pinMode(30, OUTPUT);
    pinMode(11, OUTPUT); // interrupt pin
    digitalWrite(11, 1);
    pinMode(11, INPUT); // interrupt pin
    digitalWrite(11, 1);

		#ifdef DEBUG
			Serial.println("Bulbs created.");
		#endif

		#ifdef DISPLAYTESTPATTERN
			// cycle primary colours
			SetAll(0xFF0000);
			delay(500);
			SetAll(0x00FF00);
			delay(500);
			SetAll(0x0000FF);
			delay(500);
			SetAll(0x000000);
			delay(500);

			#ifdef DEBUG
					Serial.println("Test pattern 1 done.");
			#endif

			// cycle bulbs
			byte b = 1;
      _colour = 0xFF0000;
			for (int i = 0; i < BULBS; i++)
			{
				Display(b);
				delay(200);
				b = b << 1;
			}
      Display(0);

			#ifdef DEBUG
					Serial.println("Test pattern 2 done.");
			#endif

			b = 128;
      _colour = 0x00FF00;
			for (int i = 0; i < 8; i++)
			{
				Display(b);
				delay(200);
				b = b >> 1;
			}
      Display(0);

			#ifdef DEBUG
					Serial.println("Test pattern 3 done.");
			#endif

		#endif

	}

void LightSpinnerString::SetAll(unsigned long colour)
{
	unsigned long oldcolour = _colour;
	
	_colour = colour;
	Display(0xFF);
	_colour = oldcolour;
}

DEFINE_WS2811_FN(MYWS2811_7, PORTC, 7) //30

void LightSpinnerString::Display(byte pattern)
{
	#ifdef DEBUG
//			Serial.println("Displaying a pattern.");
			Serial.println(pattern);
	#endif

	byte bulbdata[BULBS*3];
  bulbdata[0] = 0x00;
  bulbdata[1] = 0x00;
  bulbdata[2] = 0x00;
  
	byte mask = 128; // used so we can just look at the bit for the current bulb

	// for each bulb
	for (int i = 1; i < BULBS; i++)
	{
		// if the bit is set
		if (pattern & mask)
		{
			bulbdata[i*3] = RED;
			bulbdata[i*3+1] = GREEN;
			bulbdata[i*3+2] = BLUE;
		}
		else
		{
			// clear the colour
			bulbdata[i*3] = 0x00;
			bulbdata[i*3+1] = 0x00;
			bulbdata[i*3+2] = 0x00;
		}

		// move the mask along
		mask = mask >> 1;
	}

	// display it
	MYWS2811_7((const RGB_t*)bulbdata, BULBS);
	
	// now need to wait the minimum wait time 24-50us
	delayMicroseconds(50);
}

