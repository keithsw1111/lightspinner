#ifndef _LightSpinnerString_H

#define _LightSpinnerString_H

#include "lsglobal.h"

#define BULBS 9 // number of bulbs being used
#define DISPLAYS 18 // number of display changes on each rotation
#define BETWEENDISPLAYS 1000 // microsecs to sleep between bulb displays
#define MOVEMESSAGEAMT 1 // amount to move the message on each rotation

// A string of lights
class LightSpinnerString
{
private:
	unsigned long _colour;

	#define RED (_colour >> 16)
	#define GREEN ((_colour >> 8) & 0x0000FF)
	#define BLUE (_colour & 0x0000FF)
	
	void WaitForReady(); // wait for the rotation to hit the start point - this reads a switch or other trigger which goes off once per rotation at a known point and is used to manage timing of the display
	void WaitForNotReady(); // wait for the rotation to hit the start point - this reads a switch or other trigger which goes off once per rotation at a known point and is used to manage timing of the display
	void Display(byte pattern); // display a pattern
	void SetAll(unsigned long colour);

public:
	void Construct();
	void SetColour(unsigned long colour);
	void Run(byte* msg, short msglen);
};

#endif // _LightSpinnerString_H
