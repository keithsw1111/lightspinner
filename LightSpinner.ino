#include "lsglobal.h"
#include "LightSpinnerString.h"

#include "data.h"

// GLOBALS
byte lastBit = 0;
LightSpinnerString ls;
byte* message = NULL;
#define DISPLAYSPERCHAR 6

void error(int num)
{
  while(true)
  {
    for (int i = 0; i < num; i++)
    {
      digitalWrite(13, 1);
      delay(200);  
      digitalWrite(13, 1);
      delay(200);  
    }
    delay(2000);
  }
}

void LoadMessage()
{
  #ifdef DEBUG
    Serial.println("Loading Message.");
  #endif

  int msglen = 0;
  char p = pgm_read_byte_near(msg + msglen);
  while (p != 0)
  {
    p = pgm_read_byte_near(msg + msglen);
    msglen++;
  }

	message = (byte*)malloc((msglen + 2 * DISPLAYS/DISPLAYSPERCHAR) * DISPLAYSPERCHAR);
  memset(message, 0x00, (msglen + 2 * DISPLAYS/DISPLAYSPERCHAR) * DISPLAYSPERCHAR);

	if (message == NULL)
	{
		error(5);
	}

	byte* curr = message;
  int pc = 0; 
	p = pgm_read_byte_near(msg + pc);

	// start with 4 blanks
	for(byte i = 0; i < DISPLAYS/DISPLAYSPERCHAR; i++)
	{
    curr += 6;
	}

	while(p != 0)
	{
    #ifdef DEBUG
       Serial.print(F("Character read "));
       Serial.println(p);
    #endif
  
		for (int i = 0; i < DISPLAYSPERCHAR; i++)
		{
		  *curr = pgm_read_byte_near(data + i + (p - offset) * DISPLAYSPERCHAR);
			curr++;
		}
    pc++;
    p = pgm_read_byte_near(msg + pc);
	}

	// end with 4 blanks
	for(int i = 0; i < DISPLAYS/DISPLAYSPERCHAR; i++)
	{
    curr += 6;
	}
}

void setup()
{
	// initialise our error pin. This pin will be flashed if an error occurs
	pinMode(13, OUTPUT);
	digitalWrite(13, 0);

	#ifdef DEBUG
		Serial.begin(115200);
		Serial.println("In setup.");
	#endif

	ls.Construct();

	#ifdef DEBUG
		Serial.println("After LightString creation.");
	#endif

	LoadMessage();

	#ifdef DEBUG
		Serial.println("Setup done.");
	#endif
}

void loop()
{
	#ifdef DEBUG
		Serial.println("About to run.");
	#endif

	// run our program
	ls.Run(message, (strlen(msg) + 2 * DISPLAYS/DISPLAYSPERCHAR)*DISPLAYSPERCHAR);
}
