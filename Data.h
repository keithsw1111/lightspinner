#ifndef _DATA_H
#define _DATA_H

#include <avr/pgmspace.h>

byte offset = 32;

const char data[] PROGMEM = {
0,0,0,0,0,0, // space
0,0,123,123,0,0, // !
0,0,5,2,5,0, // "
0,0,5,2,5,0, // #
0,0,5,2,5,0, // $
0,0,5,2,5,0, // %
0,0,5,2,5,0, // &
0,0,5,2,5,0, // '
0,28,34,65,0,0, // (
0,65,34,28,0,0, // )
34,20,62,20,34,0, // * 
0,0,5,2,5,0, // +
0,0,5,2,5,0, // ,
0,0,5,2,5,0, // -
0,0,3,3,0,0, // .
3,4,8,16,96,0, // /
62,69,73,81,62,0, // 0
0,33,127,1,0,0, // 1
33,67,69,73,49,0, // 2
66,65,81,105,70,0, // 3
12,20,36,127,4,0, // 4
114,81,81,81,78,0, // 5
30,41,73,73,6,0, // 6
64,71,72,80,96,0, // 7
54,73,73,73,54,0, // 8
48,73,73,74,60,0, // 9
0,0,54,54,0,0, // :
0,0,5,2,5,0, // ;
0,0,5,2,5,0, // <
0,0,5,2,5,0, // = 
0,0,5,2,5,0, // >
0,0,5,2,5,0, // ?
0,0,5,2,5,0, // @
63,68,68,68,63,0, // A
127,73,73,73,54,0, // B
62,65,65,65,34,0, // C
127,65,65,65,62,0, // D
127,73,73,73,65,0, // E
127,72,72,72,64,0, // F
62,65,73,73,47,0, // G
127,8,8,8,127,0, // H
0,65,127,65,0,0, // I
2,1,65,126,64,0, // J
127,8,20,34,65,0, // K
127,1,1,1,1,0, // L
127,32,24,32,127,0, // M
127,16,8,4,127,0, // N
62,65,65,65,62,0, // O
127,72,72,72,48,0, // P
62,65,69,66,61,0, // Q
127,72,76,74,49,0, // R
49,73,73,73,70,0, // S
64,64,127,64,64,0, // T
126,1,1,1,126,0, // U
124,2,1,2,124,0, // V
126,1,6,1,126,0, // W
99,20,8,20,99,0, // X
112,8,7,8,112,0, // Y
67,69,73,81,97,0, // Z
0,0,5,2,5,0, // [
0,0,5,2,5,0, // \
0,0,5,2,5,0, // ]
0,0,5,2,5,0, // ^
0,0,5,2,5,0, // _
0,0,5,2,5,0, // `
0,0,5,2,5,0, // a
0,0,5,2,5,0, // b
0,0,5,2,5,0, // c
0,0,5,2,5,0, // d
0,0,5,2,5,0, // e
0,0,5,2,5,0, // f
0,0,5,2,5,0, // g
0,0,5,2,5,0, // h
0,0,5,2,5,0, // i
0,0,5,2,5,0, // j
0,0,5,2,5,0, // k
0,0,5,2,5,0, // l
0,0,5,2,5,0, // m
0,0,5,2,5,0, // n
0,0,5,2,5,0, // o
0,0,5,2,5,0, // p
0,0,5,2,5,0, // q
0,0,5,2,5,0, // r
0,0,5,2,5,0, // s
0,0,5,2,5,0, // t
0,0,5,2,5,0, // u
0,0,5,2,5,0, // v
0,0,5,2,5,0, // w
0,0,5,2,5,0, // x
0,0,5,2,5,0, // y
0,0,5,2,5,0, // z
0,0,5,2,5,0, // {
0,0,5,2,5,0, // |
0,0,5,2,5,0, // }
0,0,5,2,5,0 // ~
};

const char msg[] PROGMEM = "TUNE TO FM 105.2     FACEBOOK FB.ME/KELCHRISTMAS";

//
// **************************************************************************************

#endif // _DATA_H
